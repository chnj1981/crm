<?php
/**
* 部门管理控制器
* @date: 2017年5月4日 下午11:45:00
* @author: hxzlh
*/

namespace Admin\Controller;

class DepartmentController extends AdminController{

    /**
     * 部门列表
     * @date: 2017年5月6日 下午2:01:56
     * @author: hxz
     * @return: return
     */
    public function index($id=0){
        $tree = D('Department')->getTree(0,'id,sort,title,pid,status');
        $this->assign('tree', $tree);
        C('_SYS_GET_DEPARTMENT_TREE_', true); //标记系统获取分类树模板
        $this->meta_title = '部门管理';
        $this->display();
    }
    
    /**
     * 显示分类树，仅支持内部调
     * @param  array $tree 分类树
     * @author 麦当苗儿 <zuojiazi@vip.qq.com>
     */
    public function tree($tree = null){
        C('_SYS_GET_DEPARTMENT_TREE_') || $this->_empty();
        $this->assign('tree', $tree);
        $this->display('tree');
    }


    /**
     * 编辑部门
     * @date: 2017年5月6日 下午2:02:11
     * @author: hxz
     * @param unknown $id
     * @param number $pid
     * @return: return
     */
    public function edit($id = null, $pid = 0){
        $Department = D('Department');

        if(IS_POST){ //提交表单
            if(false !== $Department->update()){
                $this->success('编辑成功！', U('index'));
            } else {
                $error = $Department->getError();
                $this->error(empty($error) ? '未知错误！' : $error);
            }
        } else {
            $cate = '';
            if($pid){
                /* 获取上级部门信息 */
                $cate = $Department->info($pid, 'id,title,status');
                if(!($cate && 1 == $cate['status'])){
                    $this->error('指定的上级部门不存在或被禁用！');
                }
            }

            /* 获取部门信息 */
            $info = $id ? $Department->info($id) : '';

            $this->assign('info',       $info);
            $this->assign('category',   $cate);
            $this->meta_title = '编辑部门';
            $this->display();
        }
    }

    /* 新增部门 */
    public function add($pid = 0){
        $Department = D('Department');

        if(IS_POST){ //提交表单
            if(false !== $Department->update()){
                $this->success('新增成功！', U('index'));
            } else {
                $error = $Department->getError();
                $this->error(empty($error) ? '未知错误！' : $error);
            }
        } else {
            $cate = array();
            if($pid){
                /* 获取上级部门信息 */
                $cate = $Department->info($pid, 'id,title,status');
                if(!($cate && 1 == $cate['status'])){
                    $this->error('指定的上级部门不存在或被禁用！');
                }
            }

            /* 获取部门信息 */
            $this->assign('info',       null);
            $this->assign('category', $cate);
            $this->meta_title = '新增部门';
            $this->display('bumen');
        }
    }

    /**
     * 删除一个部门
     * @author huajie <banhuajie@163.com>
     */
    public function remove(){
        $cate_id = I('id');
        if(empty($cate_id)){
            $this->error('参数错误!');
        }

        //判断该部门下有没有子部门，有则不允许删除
        $child = M('Department')->where(array('pid'=>$cate_id))->field('id')->select();
        if(!empty($child)){
            $this->error('请先删除该部门下的子部门');
        }

        //判断该部门下有没有管理员
        $document_list = M('Member')->where(array('department_id'=>$cate_id))->field('id')->select();
        if(!empty($document_list)){
            $this->error('请先删除该部门下用户');
        }

        //删除该部门信息
        $res = M('Department')->delete($cate_id);
        if($res !== false){
            //记录行为
            $this->success('删除部门成功！');
        }else{
            $this->error('删除部门失败！');
        }
    }
    /**
     * 操作分类初始化
     * @param string $type
     * @author huajie <banhuajie@163.com>
     */
    public function operate($type = 'move'){
        //检查操作参数
        if(strcmp($type, 'move') == 0){
            $operate = '移动';
        }elseif(strcmp($type, 'merge') == 0){
            $operate = '合并';
        }else{
            $this->error('参数错误！');
        }
        $from = intval(I('get.from'));
        empty($from) && $this->error('参数错误！');
        
        //获取分类
        $map = array('status'=>1, 'id'=>array('neq', $from));
        $list = M('Department')->where($map)->field('id,pid,title')->select();
        
        
        //移动分类时增加移至根分类
        if(strcmp($type, 'move') == 0){
            //不允许移动至其子孙分类
            $list = tree_to_list(list_to_tree($list));
            
            $pid = M('Department')->getFieldById($from, 'pid');
            $pid && array_unshift($list, array('id'=>0,'title'=>'根分类'));
        }
        
        $this->assign('type', $type);
        $this->assign('operate', $operate);
        $this->assign('from', $from);
        $this->assign('list', $list);
        $this->meta_title = $operate.'分类';
        $this->display();
    }

    /**
     * 移动分类
     * @author huajie <banhuajie@163.com>
     */
    public function move(){
        $to = I('post.to');
        $from = I('post.from');
        $res = M('Department')->where(array('id'=>$from))->setField('pid', $to);
        if($res !== false){
            $this->success('分类移动成功！', U('index'));
        }else{
            $this->error('分类移动失败！');
        }
    }
    /**
     * 合并分类
     * @author huajie <banhuajie@163.com>
     */
    public function merge(){
        $to = I('post.to');
        $from = I('post.from');
        $Model = M('Category');
        
        //检查分类绑定的模型
        $from_models = explode(',', $Model->getFieldById($from, 'model'));
        $to_models = explode(',', $Model->getFieldById($to, 'model'));
        foreach ($from_models as $value){
            if(!in_array($value, $to_models)){
                $this->error('请给目标分类绑定' . get_document_model($value, 'title') . '模型');
            }
        }
        
        //检查分类选择的文档类型
        $from_types = explode(',', $Model->getFieldById($from, 'type'));
        $to_types = explode(',', $Model->getFieldById($to, 'type'));
        foreach ($from_types as $value){
            if(!in_array($value, $to_types)){
                $types = C('DOCUMENT_MODEL_TYPE');
                $this->error('请给目标分类绑定文档类型：' . $types[$value]);
            }
        }
        
        //合并文档
        $res = M('Document')->where(array('category_id'=>$from))->setField('category_id', $to);
        
        if($res !== false){
            //删除被合并的分类
            $Model->delete($from);
            $this->success('合并分类成功！', U('index'));
        }else{
            $this->error('合并分类失败！');
        }
        
    }

}
