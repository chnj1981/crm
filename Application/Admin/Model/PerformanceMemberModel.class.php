<?php
/**
* 业绩模型
* @date: 2017年5月4日 下午11:45:53
* @author: hxzlh
*/
namespace Admin\Model;
use Think\Model;

/**
 * 业绩模型
 * @author 麦当苗儿 <zuojiazi@vip.qq.com>
 */
class PerformanceMemberModel extends Model{

    protected $_validate = array(
        array('performance_id', 'require', '业绩不能为空', self::EXISTS_VALIDATE, 'regex', self::MODEL_BOTH),
        array('uid', 'require', '用户不能为空', self::EXISTS_VALIDATE, 'regex', self::MODEL_BOTH),
    );

    protected $_auto = array(
        array('create_time', NOW_TIME, self::MODEL_INSERT),
        array('update_time', NOW_TIME, self::MODEL_BOTH),
    );

    /**
     * 更新业绩信息
     * @return boolean 更新状态
     * @author 麦当苗儿 <zuojiazi@vip.qq.com>
     */
    public function update(){
        $data = $this->create($data);
        if(!$data){ //数据对象创建错误
            return false;
        }
        $target = I('post.monthly');
        $data['target'] = json_encode($target);
        $data['amount'] = array_sum($target);
        /* 添加或更新数据 */
        if(empty($data['id'])){
            $res = $this->add($data);
        }else{
            $res = $this->save($data);
        }

        return $res;
    }


}
