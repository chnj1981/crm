<?php
// +----------------------------------------------------------------------
// | OneThink [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.onethink.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: 麦当苗儿 <zuojiazi@vip.qq.com> <http://www.zjzit.cn>
// +----------------------------------------------------------------------
namespace Admin\Logic;

/**
 * 文档模型子模型 - 文章模型
 */
class CustomerLogic extends BaseLogic{
    /* 自动验证规则 */
    protected $_validate = array(
        
    );
    
    /**
     * 添加协作人
     * @date: 2017年5月14日 下午2:54:10
     * @author: hxz
     * @param unknown $cooperation_id
     * @param unknown $ids
     * @return: return
     */
    public function addToCooperation($cooperation_id=[], $ids=[]) 
    {
        $map['id'] = ['in',$ids];
        $data = $this->where($map)->field(['id','cooperation_id'])->select();
        $save['sponsor_uid'] = UID;
        foreach ($data as $k=>$v){
            $where['id'] = $v['id'];
            $cooperation = $v['cooperation_id'] ? str2arr(trim($v['cooperation_id'],',')) : [];
            $save['cooperation_id'] = ','.arr2str(array_flip(array_flip(array_merge($cooperation_id, $cooperation)))).',';
            $this->where($where)->save($save);
        }
        return true;
    }
    
    public function updateIds($ids,$customer_type=null,$follow_status=null) 
    {
        dump($ids);
        $map['id'] = ['in',$ids];
        if ($follow_status){
            $data['follow_status'] = $follow_status;
            $this->where($map)->save($data);
            foreach ($ids as $v){
                $follow['status'] = $follow_status;
                $follow['follow_time'] = NOW_TIME;
                $follow['customer_id'] = $v;
                $follow['content'] = get_nickname().'--批量更新客户跟进状态';
                D('Follow')->update($follow);
            }
        }
        if ($customer_type){
            $data2['customer_type'] = $customer_type;
            M('Document')->where($map)->save($data2);
        }
        return true;
    }


}
