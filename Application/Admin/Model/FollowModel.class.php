<?php
/**
* 跟进模型
* @date: 2017年5月4日 下午11:45:53
* @author: hxzlh
*/
namespace Admin\Model;
use Think\Model;

/**
 * 跟进模型
 * @author 麦当苗儿 <zuojiazi@vip.qq.com>
 */
class FollowModel extends Model{

    protected $_validate = array(
        array('customer_id', 'require', '客户不能为空', self::EXISTS_VALIDATE, 'regex', self::MODEL_BOTH),
        array('follow_time', 'require', '跟进时间不能为空', self::EXISTS_VALIDATE, 'regex', self::MODEL_BOTH),
        array('content,file_id', 'checkData', '附件或内容必须有一个', self::MUST_VALIDATE, 'callback', self::MODEL_BOTH),
    );
    
    protected function checkData($data) 
    {
        if (empty($data['content'] && empty($data['file_id']))){
            return false;
        }
        return true;
    }

    protected $_auto = array(
        array('uid', UID, self::MODEL_BOTH, self::MODEL_INSERT),
        array('create_time', NOW_TIME, self::MODEL_INSERT),
    );

    /**
     * 更新跟进信息
     * @return boolean 更新状态
     * @author 麦当苗儿 <zuojiazi@vip.qq.com>
     */
    public function update($result=[]){
        $data = $this->create($result);
        if(!$data){ //数据对象创建错误
            return false;
        }
        $data['follow_time'] = strtotime($data['follow_time']);

        /* 添加或更新数据 */
        if(empty($data['id'])){
            $res = $this->add($data);
        }else{
            $res = $this->save($data);
        }

        return $res;
    }


}
