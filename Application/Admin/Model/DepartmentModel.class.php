<?php
/**
* 部门模型
* @date: 2017年5月4日 下午11:45:53
* @author: hxzlh
*/
namespace Admin\Model;
use Think\Model;

/**
 * 部门模型
 * @author 麦当苗儿 <zuojiazi@vip.qq.com>
 */
class DepartmentModel extends Model{

    protected $_validate = array(
        array('title', 'require', '名称不能为空', self::EXISTS_VALIDATE, 'regex', self::MODEL_BOTH),
        array('title', '', '名称已经存在', self::VALUE_VALIDATE, 'unique', self::MODEL_BOTH),
    );

    protected $_auto = array(
        array('create_time', NOW_TIME, self::MODEL_INSERT),
        array('update_time', NOW_TIME, self::MODEL_BOTH),
        array('status', '1', self::MODEL_BOTH, self::MODEL_INSERT),
    );


    /**
     * 获取部门详细信息
     * @param  milit   $id 部门ID或标识
     * @param  boolean $field 查询字段
     * @return array     部门信息
     * @author 麦当苗儿 <zuojiazi@vip.qq.com>
     */
    public function info($id, $field = true){
        /* 获取部门信息 */
        $map = array();
        if(is_numeric($id)){ //通过ID查询
            $map['id'] = $id;
        }
        return $this->field($field)->where($map)->find();
    }

    public function getTree($id = 0, $field = true){
        /* 获取当前部门信息 */
        if($id){
            $info = $this->info($id,$field);
            $id   = $info['id'];
        }

        /* 获取所有部门 */
        $map  = array('status' => array('gt', -1));
        $list = $this->field($field)->where($map)->order('sort')->select();
        $list = list_to_tree($list, $pk = 'id', $pid = 'pid', $child = '_', $root = $id);

        /* 获取返回数据 */
        if(isset($info)){ //指定部门则返回当前部门极其子部门
            $info['_'] = $list;
        } else { //否则返回所有部门
            $info = $list;
        }

        return $info;
    }

    /**
     * 获取指定部门子部门ID
     * @param  string $cate 部门ID
     * @return string       id列表
     * @author 麦当苗儿 <zuojiazi@vip.qq.com>
     */
    public function getChildrenId($cate) {
        $field    = 'id,pid,title';
        $category = $this->getTree($cate, $field);
        $cates[] = $category;
        $res = tree_to_list($cates,'_');
        foreach ($res as $key => $value) {
            $ids[] = $value['id'];
        }
        return $ids;
    }

    /**
     * 获取指定部门的同级部门
     * @param  integer $id    部门ID
     * @param  boolean $field 查询字段
     * @return array
     * @author 麦当苗儿 <zuojiazi@vip.qq.com>
     */
    public function getSameLevel($id, $field = true){
        $info = $this->info($id, 'pid');
        $map = array('pid' => $info['pid'], 'status' => 1);
        return $this->field($field)->where($map)->order('sort')->select();
    }

    /**
     * 更新部门信息
     * @return boolean 更新状态
     * @author 麦当苗儿 <zuojiazi@vip.qq.com>
     */
    public function update(){
        $data = $this->create();
        if(!$data){ //数据对象创建错误
            return false;
        }

        /* 添加或更新数据 */
        if(empty($data['id'])){
            $res = $this->add();
        }else{
            $res = $this->save();
        }

        return $res;
    }


}
