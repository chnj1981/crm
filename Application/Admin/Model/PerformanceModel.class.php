<?php
/**
* 业绩模型
* @date: 2017年5月4日 下午11:45:53
* @author: hxzlh
*/
namespace Admin\Model;
use Think\Model;

/**
 * 业绩模型
 * @author 麦当苗儿 <zuojiazi@vip.qq.com>
 */
class PerformanceModel extends Model{

    protected $_validate = array(
        array('year', 'require', '年份不能为空', self::EXISTS_VALIDATE, 'regex', self::MODEL_BOTH),
        array('type', 'require', '类型不能为空', self::EXISTS_VALIDATE, 'regex', self::MODEL_BOTH),
        array('type,year', 'checkData', '业绩不能重复', self::MUST_VALIDATE, 'callback', self::MODEL_BOTH),
    );
    
    protected function checkData($data) 
    {
        return $this->where($data)->find() ? false : true;
    }

    protected $_auto = array(
        array('create_time', NOW_TIME, self::MODEL_INSERT),
        array('update_time', NOW_TIME, self::MODEL_BOTH),
        array('status', '1', self::MODEL_BOTH, self::MODEL_INSERT),
    );

    /**
     * 更新业绩信息
     * @return boolean 更新状态
     * @author 麦当苗儿 <zuojiazi@vip.qq.com>
     */
    public function update(){
        $data = $this->create();
        if(!$data){ //数据对象创建错误
            return false;
        }

        /* 添加或更新数据 */
        if(empty($data['id'])){
            $res = $this->add();
        }else{
            $res = $this->save();
        }

        return $res;
    }


}
