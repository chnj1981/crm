<?php
/**
* 业绩管理控制器
* @date: 2017年5月4日 下午11:45:00
* @author: hxzlh
*/

namespace Admin\Controller;

class PerformanceController extends AdminController{

    /**
     * 业绩列表
     * @date: 2017年5月6日 下午2:01:56
     * @author: hxz
     * @return: return
     */
    public function index(){
        $map  = array('status' => 1);

        $list = $this->lists('Performance', $map);
        $this->assign('_list', $list);
        $this->meta_title = '业绩管理';
        $this->display();
    }

    /**
     * 业绩设置
     * @date: 2017年5月10日 下午11:27:09
     * @author: hxzlh
     * @param unknown $param
     * @return: return
     */
    public function goal() 
    {
        $id = I('get.id');
        empty($id) && $this->error('参数不能为空！');
        $this->assign('performance_id',$id);
        //所属部门
        $department = M('department')->field(true)->select();
        $department = D('Common/Tree')->toFormatTree($department);
        $department = array_merge(array(0=>array('id'=>0,'title_show'=>'顶级')), $department);
	 
        $this->assign('Department', $department);
        
        $departmentId= I('get.department_id');
        if ($departmentId){
            $ids = D('Department')->getChildrenId($departmentId);
            if ($ids)
                $map['department_id'] = ['in',$ids];
        }
        $map['status'] = 1;
        $list   = $this->lists('Member', $map);
        foreach ($list as $k=>$v){
            $map['uid'] = $v['uid'];
            $map['performance_id'] = $id;
            $target = D('PerformanceMember')->where($map)->find();
            $list[$k]['amount'] = $target['amount'] ?: 0;
            $list[$k]['monthly'] = $target['target'] ? json_decode($target['target'],true) : [0,0,0,0,0,0,0,0,0,0,0,0];
        }
        
        $this->assign('_list', $list);
        $this->meta_title = '用户信息';
        $this->display();
        
    }
    //修改业绩设定
    public function editGoal() 
    {
        $per = D('PerformanceMember');
        if(IS_POST){ //提交表单
            if(false !== $per->update()){
                $this->success('编辑成功！', U('index'));
            } else {
                $error = $per->getError();
                $this->error(empty($error) ? '未知错误！' : $error);
            }
        } else {
            $id = I('get.id');
            empty($id) && $this->error('参数不能为空！');
            $uid = I('get.uid');
            empty($uid) && $this->error('参数不能为空！');
            
            /* 获取业绩信息 */
            $map['uid'] = $uid;
            $map['performance_id'] = $id;
            $info= D('PerformanceMember')->where($map)->find();
            $info['target'] = $info['target'] ? json_decode($info['target'],true) : [0,0,0,0,0,0,0,0,0,0,0,0];
            $info['uid'] = $uid;
            $info['performance_id'] = $id;
            $this->assign('info',       $info);
            $this->meta_title = '编辑业绩';
            $this->display();
        }
    }

    /**
     * 编辑业绩
     * @date: 2017年5月6日 下午2:02:11
     * @author: hxz
     * @param unknown $id
     * @param number $pid
     * @return: return
     */
    public function edit($id = null, $pid = 0){
        $Performance = D('Performance');

        if(IS_POST){ //提交表单
            if(false !== $Performance->update()){
                $this->success('编辑成功！', U('index'));
            } else {
                $error = $Performance->getError();
                $this->error(empty($error) ? '未知错误！' : $error);
            }
        } else {
            /* 获取业绩信息 */
            $info = $id ? $Performance->find($id) : '';

            $this->assign('info',       $info);
            $this->meta_title = '编辑业绩';
            $this->display();
        }
    }

    /* 新增业绩 */
    public function add($pid = 0){
    	
        $Performance = D('Performance');
        if(IS_POST){ //提交表单
            if(false !== $Performance->update()){
                $this->success('新增成功！', U('index'));
            } else {
                $error = $Performance->getError();
                $this->error(empty($error) ? '未知错误！' : $error);
            }
        } else {
            $this->assign('info',       null);
            $this->meta_title = '新增业绩';
            $this->display('edit');
        }
    }
    
    public function changeTop($method=null){
        $id = array_unique((array)I('id',0));
        $id = is_array($id) ? implode(',',$id) : $id;
        if ( empty($id) ) {
            $this->error('请选择要操作的数据!');
        }
        $map['id'] =   array('in',$id);
        switch ( strtolower($method) ){
            case 'top'://置顶
                $data['is_top'] = 1;
                break;
            case 'cancel'://取消置顶
                $data['is_top'] = 0;
                break;
            default:
                $this->error('参数非法');
        }
        $notice = D('Performance');
        $notice->create($data,2);
        $res = $notice->where($map)->save();
        dump($res);
    }

    /**
     * 删除一个业绩
     * @author huajie <banhuajie@163.com>
     */
    public function remove(){
        $notice_id = I('id');
        if(empty($notice_id)){
            $this->error('参数错误!');
        }

        //删除该业绩信息
        $res = M('Performance')->delete($notice_id);
        if($res !== false){
            //记录行为
            $this->success('删除业绩成功！');
        }else{
            $this->error('删除业绩失败！');
        }
    }



}
